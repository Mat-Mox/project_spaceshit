﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class VisualAlertGenerator : MonoBehaviour
{

    [SerializeField]
    private Material initmat;

    [SerializeField]
    private Material successmat;

    public void GenerateVisualAlert(string alertMessage)
    {
        GetComponentInChildren<TextMeshPro>().text = alertMessage;
    }


}
