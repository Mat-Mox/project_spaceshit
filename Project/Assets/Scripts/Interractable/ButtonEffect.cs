﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============

using System;
using System.Collections;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class ButtonEffect : MonoBehaviour
{
    private ProceduralGeneration pgen;

    private Color initialColor;
    private new Renderer renderer;

    [SerializeField] private float nonInteractableDelay = 1f;
    private bool locked;

    [SerializeField] private GameObject particles;

    [SerializeField] private AudioClip pressSound;
    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        pgen = FindObjectOfType<ProceduralGeneration>();
        renderer = GetComponentInChildren<Renderer>();
        initialColor = renderer.material.color;
    }

    public void OnButtonDown(Hand fromHand)
    {
        if (locked) return;
        pgen.CheckScenario(gameObject);
        ColorSelf(Color.cyan);
        StartCoroutine(Lock());

        particles.SetActive(false);
        particles.SetActive(true);

        audioSource.PlayOneShot(pressSound);

        fromHand.TriggerHapticPulse(1000);
    }

    private IEnumerator Lock()
    {
        locked = true;
        yield return new WaitForSeconds(nonInteractableDelay);
        locked = false;
    }

    public void OnButtonUp(Hand fromHand)
    {
        ColorSelf(initialColor);
    }

    private void ColorSelf(Color newColor)
    {
        renderer.material.color = newColor;
    }
}