﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class Lever : MonoBehaviour
{
    private ProceduralGeneration pgen;

    private bool grabbed = false;

    private bool smoothRelease = false;

    [SerializeField] private Transform startPos;

    private float posToGoX;
    private float posToGoY;
    private float posToGoZ;

    private Vector3 PosToGo;

    [SerializeField] private float speed = 0.1f;

    [SerializeField] private GameObject particles;

    private void Start()
    {
        pgen = FindObjectOfType<ProceduralGeneration>();
        posToGoX = startPos.transform.position.x;
        posToGoY = startPos.transform.position.y;
        posToGoZ = startPos.transform.position.z;

        PosToGo = new Vector3(posToGoX, posToGoY, posToGoZ);
    }

    private void Update()
    {
        if (transform.parent.GetComponentInChildren<LinearMapping>().value >= 0.95f && GetComponent<Interactable>().interactible && !grabbed)
        {
            Debug.Log("no interractible");
            GetComponent<Interactable>().interactible = false;
            pgen.CheckScenario(transform.parent.gameObject);
            particles.SetActive(false);
            particles.SetActive(true);
            smoothRelease = true;
            StartCoroutine(WaitBeforeInitPos());
        }

            if (!grabbed && !smoothRelease)
        {
            transform.position = PosToGo;
            GetComponentInChildren<LinearDrive>().UpdateLinearMapping(transform);
        }
    }

    public void OnLeverAction()
    {
        grabbed = false;
    }

    public void GrabLever()
    {
        grabbed = true;
    }

    private IEnumerator WaitBeforeInitPos()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        GetComponent<Interactable>().interactible = true;
        yield return new WaitForSecondsRealtime(0.5f);
        smoothRelease = false;
        grabbed = false;
    }
}