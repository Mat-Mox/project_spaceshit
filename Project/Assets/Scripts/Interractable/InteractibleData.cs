﻿using UnityEngine;

public class InteractibleData : MonoBehaviour
{
    public Generator.ColorAndNames data;

    public new string name;

    [SerializeField] private new Renderer renderer;
    [SerializeField] private string colorPropertyName = "_Color";

    public void Initialize(Generator.ColorAndNames cn)
    {
        renderer.material.SetColor(colorPropertyName, cn.color);
        data = cn;
    }
}