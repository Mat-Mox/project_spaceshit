﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class Wheel : MonoBehaviour
{
    private ProceduralGeneration pgen;

    private CircularDrive cdrive;

    private Quaternion initRotation;

    [SerializeField] private GameObject particles;

    private void Start()
    {
        pgen = FindObjectOfType<ProceduralGeneration>();
        cdrive = GetComponent<CircularDrive>();
        initRotation = transform.rotation;
    }

    private void Update()
    {
        if (Mathf.Abs(cdrive.outAngle) >= 175)
        {
            transform.rotation = initRotation;
            cdrive.outAngle = initRotation.eulerAngles.y;
            StartCoroutine(StopInteraction());
            OnFreezeValue();
        }
    }

    public void OnFreezeValue()
    {
        pgen.CheckScenario(transform.parent.gameObject);

        particles.SetActive(false);
        particles.SetActive(true);
    }

    private IEnumerator StopInteraction()
    {
        GetComponent<Interactable>().interactible = false;
        yield return new WaitForSecondsRealtime(1);
        GetComponent<Interactable>().interactible = true;
    }
}