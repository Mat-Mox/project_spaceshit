﻿using TMPro;
using UnityEngine;

public class ShutdownEvent : MonoBehaviour
{
    [SerializeField] private float delayMin = 40f;
    [SerializeField] private float delayMax = 120f;
    private float delay;
    [SerializeField] private GameObject buttonGlow;
    [SerializeField] private Light emergencyLight;
    [SerializeField] private AudioSource alarm;

    private float timer;

    private Light[] lights;

    private bool shutdown;

    private TimerScreen tscreen;
    private VisualAlertGenerator vgen;

    private void Start()
    {
        tscreen = FindObjectOfType<TimerScreen>();
        vgen = FindObjectOfType<VisualAlertGenerator>();
        lights = FindObjectsOfType<Light>();
        ResetDelay();

        if (!alarm)
        {
            Debug.LogError("No alarm audioSource");
        }
    }

    private void Update()
    {
        if (shutdown)
        {
            //tscreen.text.enabled = false;
            vgen.GetComponentInChildren<TextMeshPro>().enabled = false;
            return;
        } else
        {
        }

        timer += Time.deltaTime;

        if (timer >= delay)
        {
            shutdown = true;
            foreach (Light sceneLight in lights)
            {
                sceneLight.enabled = false;
            }

            emergencyLight.enabled = true;
            buttonGlow.SetActive(true);
            
            alarm.Play();
        }
    }

    public void OnButtonPressed()
    {
        if (!shutdown) return;

        foreach (Light sceneLight in lights)
        {
            sceneLight.enabled = true;
        }

        emergencyLight.enabled = false;
        buttonGlow.SetActive(false);
        shutdown = false;
        timer = 0f;
        ResetDelay();
        alarm.Stop();
    }

    private void ResetDelay()
    {
        delay = Random.Range(delayMin, delayMax);
    }
}