﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class TimerScreen : MonoBehaviour
{
    public TextMeshPro text;
    private float timer;
    [SerializeField] private float initialTimer = 30f;

    [SerializeField] [ColorUsage(true, true)]
    private Color initialColor;

    [SerializeField] [ColorUsage(true, true)]
    private Color alertColor;

    [SerializeField] private float gradientThreshold = 5f;
    [SerializeField] private float gradientDuration = .5f;

    [SerializeField] private Renderer[] clockRenderers;

    public UnityAction timeOut;
    private static readonly int EmissionColor = Shader.PropertyToID("_EmissionColor");

    [SerializeField] [Range(0, 10)] private int baseLives = 3;
    private ProceduralGeneration proceduralGeneration;
    public int CurrentLives { get; private set; }

    private AudioSource audioSource;
    [SerializeField] private AudioClip chronoSound;

    private bool chronoSoundTriggered;

    [SerializeField] private List<GameObject> UILife = new List<GameObject>();

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        timer = initialTimer;
        ResetColor();
        CurrentLives = baseLives;

        proceduralGeneration = FindObjectOfType<ProceduralGeneration>();
    }

    private void Update()
    {
        timer -= Time.deltaTime;
        int seconds = (int) timer;
        int hundreths = (int) ((timer - seconds) * 100);

        string textContent = "";
        if (seconds < 10) textContent += "0";
        textContent += seconds + ".";
        if (hundreths < 10) textContent += "0";
        textContent += hundreths;
        text.text = textContent;

        if (timer <= gradientThreshold && timer >= gradientThreshold - gradientDuration)
        {
            if (!chronoSoundTriggered)
            {
                audioSource.PlayOneShot(chronoSound);
                chronoSoundTriggered = true;
            }

            foreach (Renderer clockRenderer in clockRenderers)
            {
                clockRenderer.material.SetColor(EmissionColor,
                    Color.Lerp(initialColor, alertColor, (gradientThreshold - timer) / gradientDuration));
            }
        }
        else if (timer < gradientThreshold)
        {
            foreach (Renderer clockRenderer in clockRenderers)
            {
                clockRenderer.material.SetColor(EmissionColor, alertColor);
            }
        }

        if (timer <= 0f)
        {
            timeOut?.Invoke();
            timer = initialTimer;
            ResetColor();

            --CurrentLives;
            UILife[CurrentLives].SetActive(true);
            Debug.Log("-1 life");
            if (CurrentLives <= 0)
            {
                Debug.Log("YOU LOSE");
                SceneManager.LoadScene("MenuScene");
            }

            proceduralGeneration.SetNextScenario();
        }
    }

    private void ResetColor()
    {
        foreach (Renderer clockRenderer in clockRenderers)
        {
            clockRenderer.material.SetColor(EmissionColor, initialColor);
        }
    }

    public void OnError()
    {
        timer -= initialTimer / 4f;
        if (timer < 0f)
        {
            timer = 0f;
        }
    }

    public void NewRound()
    {
        --initialTimer;
        if (initialTimer < 5f) initialTimer = 5f;
        timer = initialTimer;
        audioSource.Stop();
        chronoSoundTriggered = false;
    }
}