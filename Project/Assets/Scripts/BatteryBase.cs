﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class BatteryBase : MonoBehaviour
{
    [SerializeField] private Transform snapPoint;
    [SerializeField] private float ejectImpulseStrength = 5f;
    [SerializeField] private float ejectAngle = 10f;
    [SerializeField] private float uninteractableDelay = .5f;

    [SerializeField] private bool autoEject = true;
    [SerializeField] private float autoReleaseDelay = 0.25f;

    private Battery basedBattery;

    private ProceduralGeneration pgen;

    private InteractibleData interactibleData;

    [SerializeField] private GameObject particles;

    [SerializeField] private AudioClip plugSound;
    [SerializeField] private AudioClip releaseSound;
    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        pgen = FindObjectOfType<ProceduralGeneration>();
        interactibleData = GetComponentInParent<InteractibleData>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (basedBattery) return;

        Battery battery = other.GetComponent<Battery>();

        if (!battery || battery.IsBased) return;

        InteractibleData batteryInteractibleData = other.GetComponentInParent<InteractibleData>();
        if (batteryInteractibleData.data.color != interactibleData.data.color) return;

        battery.IsBased = true;

        battery.Root.position = snapPoint.position;
        battery.Root.rotation = Quaternion.identity;

        audioSource.PlayOneShot(plugSound);

        basedBattery = battery;

        particles.SetActive(false);
        particles.SetActive(true);

        pgen.CheckScenario(transform.parent.gameObject);

        if (autoEject)
        {
            StartCoroutine(AutoRelease());
        }
    }

    private IEnumerator AutoRelease()
    {
        yield return new WaitForSeconds(autoReleaseDelay);
        Release();
    }

    public void Release()
    {
        if (!basedBattery) return;

        basedBattery.IsBased = false;
        basedBattery.Root.rotation = Quaternion.Euler(ejectAngle, Random.Range(0f, 360f), 0f);
        basedBattery.Rigidbody.AddForce(basedBattery.transform.up * ejectImpulseStrength, ForceMode.Impulse);
        audioSource.PlayOneShot(releaseSound);
        StartCoroutine(ResetBattery());
    }

    private IEnumerator ResetBattery()
    {
        yield return new WaitForSeconds(uninteractableDelay);
        basedBattery = null;
    }
}