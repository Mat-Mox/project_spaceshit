﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
using Random = UnityEngine.Random;

public class Battery : MonoBehaviour
{
    [SerializeField] private Transform root;
    private Interactable interactable;
    private Throwable throwable;
    private new Rigidbody rigidbody;

    public Rigidbody Rigidbody => rigidbody;

    public Transform Root => root;

    private bool isBased;

    public Action based;

    public bool IsBased
    {
        get => isBased;
        set
        {
            interactable.enabled = !value;
            throwable.enabled = !value;
            rigidbody.isKinematic = value;
            if (value)
            {
                rigidbody.velocity = Vector3.zero;
            }

            if (value && !isBased)
            {
                based?.Invoke();
            }

            isBased = value;
        }
    }

    private void Awake()
    {
        interactable = root.GetComponent<Interactable>();
        throwable = root.GetComponent<Throwable>();
        rigidbody = root.GetComponent<Rigidbody>();
        
    }
}